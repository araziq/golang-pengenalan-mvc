package main

import (
	"context"
	"log"
	"net/http"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/db"
	"github.com/gin-gonic/gin"
	"google.golang.org/api/option"
)

var client *db.Client
var ctx context.Context

func init() {
	ctx = context.Background()
	conf := &firebase.Config{
		DatabaseURL: "https://proyek-pwa-6f540.firebaseio.com",
	}
	// Fetch the service account key JSON file contents
	opt := option.WithCredentialsFile("proyek-pwa-6f540-firebase-adminsdk-hmsv1-85e88b5e4c.json")

	// Initialize the app with a service account, granting admin privileges
	app, err := firebase.NewApp(ctx, conf, opt)
	if err != nil {
		log.Fatalln("Error initializing app:", err)
	}

	client, err = app.Database(ctx)
	if err != nil {
		log.Fatalln("Error initializing database client:", err)
	}
}

func main() {
	router := gin.Default()
	router.GET("/data", getDaftarHadirHandler)
	router.Run(":3030")
}

// DaftarHadir : Struktur Data db
type DaftarHadir struct {
	UserID string `json:"userId"`
	Kota   string `json:"kota"`
	Nama   string `json:"nama"`
}

// GetDaftarHadir : Mengambil daftar hadir
func GetDaftarHadir() (bool, map[string]DaftarHadir, error) {
	var Daftar map[string]DaftarHadir
	ref := client.NewRef("daftarHadir")
	if err := ref.Get(ctx, &Daftar); err != nil {
		log.Fatalln(err)
		return false, Daftar, err
	}
	return true, Daftar, nil
}

func getDaftarHadirHandler(c *gin.Context) {
	flag, resp, err := GetDaftarHadir()
	if flag {
		c.JSON(http.StatusOK, map[string]interface{}{
			"status": "success",
			"data":   resp,
		})
	} else {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status": "failed",
			"error":  err,
		})
	}
}
